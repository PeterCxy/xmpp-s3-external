#!/usr/bin/env python
# HMAC_SHA256 implemented in Python
# used to test our implementation for signature verification
# usage: hmac_sha256.py [key] [str]
# in [str], all ';' will be replaced with '\0'
import hmac
import hashlib
import sys

signature = hmac.new(bytes(sys.argv[1] , 'utf-8'), msg = bytes(sys.argv[2].replace(";", "\0"), 'utf-8'), digestmod = hashlib.sha256).hexdigest()
print(signature)