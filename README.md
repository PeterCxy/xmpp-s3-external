This is a `mod_http_upload_external`-compatible XEP-0363 file server for Prosody and eJabberd implemented on Cloudflare Workers with S3-compatible backend support.

To use it, you need to fill in `config.prod.json` using `config.example.json` as a template, and then create `wrangler.prod.toml` based on `wrangler.example.toml`. Most of the fields should be pretty self-explanatory. To publish it to your Cloudflare account, run `npm run publish-prod`.

To configure eJabberd to use the endpoint exposed by this service, add

```yaml
  mod_http_upload:
    put_url: https://<worker_url>/
    get_url: https://<worker_url>/
    external_secret: <secret_in_config_json>
```

to `ejabberd.yaml`.