const path = require('path')
const { config } = require('process')

module.exports = {
  entry: './src/index.coffee',
  output: {
    filename: 'worker.js',
    path: path.join(__dirname, 'dist'),
  },
  mode: 'production',
  resolve: {
    extensions: ['.coffee', '.js'],
    fallback: { "os": false },
    alias: {
      config: path.resolve(__dirname,
        process.env.BUILD_ENV == "production" ? "config.prod.json" : "config.json")
    }
  },
  module: {
    rules: [
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      },
      {
        type: 'javascript/auto',
        test: /\.json$/,
        use: [ 'json-loader' ]
      }
    ]
  }
}